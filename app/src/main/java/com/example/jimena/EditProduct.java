package com.example.jimena;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.jimena.models.Product;

import io.realm.Realm;

public class EditProduct extends AppCompatActivity {

    private EditText etName;
    private EditText etDescription;
    private EditText etQuantity;
    private EditText etValue;
    private Button btnSave;

    private Realm realm;

    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Editar producto");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                }
            });
        }
        realm = Realm.getDefaultInstance();
        loadViews();
        id = getIntent().getStringExtra("productId");
        loadProduct(id);
    }

    private void loadProduct(String id) {
        Product product = realm.where(Product.class).equalTo("id", id).findFirst();
        if (product != null) {
            etName.setText(product.getName());
            etDescription.setText(product.getDescription());
            etQuantity.setText(String.valueOf(product.getQuantify()));
            etValue.setText(String.valueOf(product.getValue()));
        }
    }

    private void loadViews() {
        etName = findViewById(R.id.etNombre);
        etDescription = findViewById(R.id.etDescripcion);
        etQuantity = findViewById(R.id.etCantidad);
        etValue = findViewById(R.id.etPrecio);
        btnSave = findViewById(R.id.btnGuardar);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProduct();
            }
        });
    }

    private void updateProduct() {
        String nameProduct = etName.getText().toString();
        String description = etDescription.getText().toString();
        String strQuantity = etQuantity.getText().toString().trim();
        String strPrice = etValue.getText().toString().trim();
        boolean isSuccess = true;

        if (nameProduct.isEmpty()) {
            String message = getString(R.string.requeried);
            etName.setError(message);
            isSuccess = false;
        }
        if (strQuantity.isEmpty()) {
            String message = getString(R.string.requeried);
            etQuantity.setError(message);
            isSuccess = false;
        }
        if (strPrice.isEmpty()) {
            String message = getString(R.string.requeried);
            etValue.setError(message);
            isSuccess = false;
        }
        if (isSuccess) {
            float price = Float.parseFloat(strPrice);
            int quantity = Integer.parseInt(strQuantity);
            Product product = new Product(id, nameProduct, description, quantity, (int) price);
            updateProductInDB(product);
            finish();
        }

    }

    private void updateProductInDB(Product product) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(product);
        realm.commitTransaction();
    }
}
