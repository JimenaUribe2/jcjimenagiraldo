package com.example.jimena;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jimena.models.Product;

import java.util.UUID;

import io.realm.Realm;

public class AddProducto extends AppCompatActivity {

    EditText etName;
    EditText etDescription;
    EditText etQuantity;
    EditText etValue;

    Button btnSave;
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_producto);
        Toolbar toolbar = findViewById(R.id.toolbar);
        loadViews();
        realm = Realm.getDefaultInstance();
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.agregarproducto);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                }
            });
        }
    }

    private void loadViews() {
        etName=findViewById(R.id.etNombre);
        etDescription=findViewById(R.id.etDescripcion);
        etQuantity=findViewById(R.id.etCantidad);
        etValue=findViewById(R.id.etPrecio);
        btnSave=findViewById(R.id.btnGuardar);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save(view);
            }
        });
    }


    private void save(View view){
        String name = etName.getText().toString().trim();
        String description = etDescription.getText().toString().trim();
        String strQuantity = etQuantity.getText().toString().trim();
        String strValue = etValue.getText().toString().trim();

        boolean isSuccess = true;

        if (name.isEmpty()) {
            String message = getString(R.string.required);
            etName.setError(message);
            isSuccess = false;
        }
        if (description.isEmpty()) {
            String message = getString(R.string.required);
            etDescription.setError(message);
            isSuccess = false;
        }
        if (strQuantity.isEmpty()) {
            String message = getString(R.string.required);
            etQuantity.setError(message);
            isSuccess = false;
        }
        if (strValue.isEmpty()) {
            String message = getString(R.string.required);
            etValue.setError(message);
            isSuccess = false;
        }
        if (isSuccess) {
            float value = Float.parseFloat(strValue);
            int quantify = Integer.parseInt(strQuantity);
            String id = UUID.randomUUID().toString();
            Product product = new Product(id, name, description, quantify, value);
            saveInDB(product);
        }
    }

    private void saveInDB(Product product) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(product);
        realm.commitTransaction();
        openMainActivity();


    }

    private void openMainActivity() {
        Toast.makeText(this, "Producto guardado", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

}

