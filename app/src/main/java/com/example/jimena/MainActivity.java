package com.example.jimena;

import android.content.Intent;
import android.os.Bundle;

import com.example.jimena.models.Product;
import com.example.jimena.models.ProductAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity implements ProductAdapter.ProductAdapterListener {

    private RecyclerView recyclerView;
    private Realm realm;
    private ProductAdapter productAdapter;
    private LinearLayout linearLayoutRecycler;
    private LinearLayout linearLayoutView;
    private TextView textViewValueTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.compras);
        }

        realm = Realm.getDefaultInstance();
        linearLayoutRecycler = findViewById(R.id.linearLayoutRecyclerView);
        linearLayoutView = findViewById(R.id.linearLayoutViewZero);
        recyclerView = findViewById(R.id.recycler_view);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                irAAnadirProducto();
            }
        });
        listSize();
        loadProducts();
    }

    private void loadProducts() {
        List<Product> products = getProducts();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        productAdapter = new ProductAdapter(this, products, this);
        recyclerView.setAdapter(productAdapter);
    }


    private void listSize() {
        List<Product> productos = getProducts();

        if (productos.size() == 0) {
            linearLayoutView.setVisibility(View.VISIBLE);
            linearLayoutRecycler.setVisibility(View.GONE);
        } else {
            linearLayoutView.setVisibility(View.GONE);
            linearLayoutRecycler.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        updateProduts();
    }

    private void updateProduts() {
        productAdapter.updateProducts(getProducts());
    }


    private List<Product> getProducts() {
        textViewValueTotal = findViewById(R.id.tv_totalValue);
        List<Product> products = realm.where(Product.class).findAll();
        float totVal = 0;
        for (int i = 0; i<products.size() ; i++) {
            Product product = products.get(i);
            if (product != null) {
                totVal = totVal +(product.getQuantify() * product.getValue());
            }
        }
        textViewValueTotal.setText(String.valueOf(totVal));
        return products;

    }

    private void irAAnadirProducto() {
        Intent intent = new Intent( this, AddProducto.class);
        startActivity(intent);
    }


    @Override
    public void deleteProduct(String id) {
        realm.beginTransaction();
        Product product = realm.where(Product.class).equalTo("id", id).findFirst();
        if (product != null) {
            product.deleteFromRealm();
        }
        realm.commitTransaction();
        Toast.makeText(MainActivity.this, "Borrado correctamente", Toast.LENGTH_LONG).show();
        updateProduts();
        listSize();
    }

    @Override
    public void editProduct(String id) {
        Intent intent = new Intent(this, EditProduct.class);
        intent.putExtra("productId", id);
        startActivity(intent);

    }
}