package com.example.jimena.models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jimena.MainActivity;
import com.example.jimena.R;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ItemViewHolder> {

    private Context context;
    private List<Product> items;
    private ProductAdapterListener listener;

    public ProductAdapter(Context context, List<Product> items, MainActivity listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ProductAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View template = inflater.inflate(R.layout.product_template, parent, false);
        return new ProductAdapter.ItemViewHolder(template);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final Product product = items.get(position);
        float totalBuyValue = product.getQuantify()*product.getValue();
        holder.tvName.setText(product.getName());
        holder.tvDescription.setText(product.getDescription());
        holder.tvQuantity.setText(String.valueOf(product.getQuantify()));
        holder.tvValue.setText(String.valueOf(product.getValue()));
        holder.tvBuyTotalValue.setText(String.valueOf(totalBuyValue));
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.deleteProduct(product.getId());
            }
        });
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.editProduct(product.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void updateProducts(List<Product> products) {
        this.items = products;
        notifyDataSetChanged();
    }

    public void setProducts(List<Product> productsFiltered) {
        this.items = productsFiltered;
        notifyDataSetChanged();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvDescription;
        private TextView tvQuantity;
        private TextView tvValue;
        private TextView tvBuyTotalValue;
        private ImageView ivEdit;
        private ImageView ivDelete;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_nombre);
            tvDescription = itemView.findViewById(R.id.tv_descripcion);
            tvQuantity = itemView.findViewById(R.id.tv_cantidad);
            tvValue = itemView.findViewById(R.id.tv_precio);
            tvBuyTotalValue = itemView.findViewById(R.id.tv_total);
            ivEdit = itemView.findViewById(R.id.iv_editar);
            ivDelete = itemView.findViewById(R.id.iv_eliminar);
        }
    }

    public interface ProductAdapterListener {

        void deleteProduct(String id);

        void editProduct(String id);
    }
}

